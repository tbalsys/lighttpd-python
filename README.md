# lighttpd Docker image

Docker image with python and web server [lighttpd](http://www.lighttpd.net/) and configured cgi-bin directory

### Start a container with Docker

With the default configuration files:

```sh
docker run --rm -t \
  -v <home-directory>:/var/www/localhost/htdocs \
  -v <cgi-bin-directory>:/var/www/localhost/cgi-bin \
  -p <http-port>:80 \
  quay.io/tbalsys/lighttpd-python
```

With custom configuration files:

```sh
docker run --rm -t \
  -v <home-directory>:/var/www/localhost/htdocs \
  -v <cgi-bin-directory>:/var/www/localhost/cgi-bin \
  -v <config-directory>:/etc/lighttpd \
  -p <http-port>:80 \
  quay.io/tbalsys/lighttpd-python
```

### Start a container with Docker Compose

Add the following lines in an existing or a new `docker-compose.yml` file:

```yaml
  lighttpd:
    image: quay.io/tbalsys/lighttpd-python
    volumes:
      - <home-directory>:/var/www/localhost/htdocs
      - <cgi-bin-directory>:/var/www/localhost/cgi-bin
      - <config-directory>:/etc/lighttpd
    ports:
      - "<http-port>:80"
    tty: true
```

**Note** – The `- <config-directory>:…` line is optional

Then start a lighttpd container with:

```sh
docker-compose up lighttpd
```

### Build with Docker

This command will build the image:

```
docker build .
```

### Build with Docker Compose

Build the image with this command:

```
docker-compose build
```
