FROM alpine:3.12

RUN apk add  --update --no-cache lighttpd python2 && \
  rm -rf /var/cache/apk/* && \
  sed -i /etc/lighttpd/lighttpd.conf \
    -e 's/.*include "mod_cgi.conf"/include "mod_cgi.conf"/' \
    -e 's/static-file.exclude-extensions.*/static-file.exclude-extensions = (".py")/' \
    -e 's/#.*"mod_alias"/    "mod_alias"/' \
    -e 's#^accesslog.filename.*#accesslog.filename   = "/tmp/logpipe"#' \
    -e 's#^server.errorlog.*#server.errorlog      = "/tmp/logpipe"#' && \
  sed -i /etc/lighttpd/mod_cgi.conf \
    -e 's#".cgi".*#".py"   =>      "/usr/bin/python"#'

EXPOSE 80

CMD ["sh", "-c", "if [ ! -e /tmp/logpipe ]; then mkfifo -m 600 /tmp/logpipe; fi; \
(cat <> /tmp/logpipe 1>&2 &); \
chown lighttpd:lighttpd /tmp/logpipe; \
exec lighttpd -D -f /etc/lighttpd/lighttpd.conf"]

#CMD ["lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
